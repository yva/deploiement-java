# Généralités

Pour créer un projet Spring Boot "from scratch": http://start.spring.io

## Profils Spring

Il en existe deux explicites:

* dev
* prod

Et un de fait:

* default (`application.properties`)

Les règles de priorité de prise en compte des propriétés sont définies
ici: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html

Selon les modules activés Spring vous permet de configurer
rapidement votre application. Une liste non exhaustive des possiblités
est disponible ici: https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html

## Profil Maven

Sur ce projet nous avons créé un **profil** de "build" Maven nommé **prod**.
Ce profil permet de dire que nous voulons générer un **war** à déployer sur un
conteneur de servlet / serveur d'application ultérieurement.

Il se charge alors de définir le packaging comme war et surtout de définir
la dépendance à Tomcat comme `provided`. Sinon c'est comme si nous avions packagé
un Tomcat à exécuter dans un Tomcat... **#INCEPTION**

Si le **profil prod n'est pas sélectionné** alors le build créer un **jar** autoportant (ou uberjar).
Ce livrable embarque un Tomcat, il n'a donc plus besoin d'un serveur pour s'exécuter. 
Les modalités de lancement sont définies dans `deploiement/launch.bat|sh`.

## Paramétrage Tomcat

Tout ce qu'il y a dans l'arborescence `tomcat/`.
