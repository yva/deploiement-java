package fr.laerce.greta.demo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by yvarni on 22/06/2017.
 */
public interface TrackerRepository extends JpaRepository<Tracker, Long> {
}
