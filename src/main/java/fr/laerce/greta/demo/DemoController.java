package fr.laerce.greta.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yvarni on 22/06/2017.
 */
@Controller
public class DemoController {

    @Value("${demo.message}")
    private String message;

    @Value("${tartempion:defaut}")
    private String tartempion;

    @Autowired
    private TrackerRepository repository;

    @RequestMapping("/")
    public String index(Model model, HttpServletRequest request) {
        repository.save(new Tracker("/", request.getSession().getId()));

        model.addAttribute("trackers", repository.findAll());
        model.addAttribute("message", message);
        model.addAttribute("tartempion", tartempion);

        return "home";
    }
}
