package fr.laerce.greta.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by yvarni on 22/06/2017.
 */
@Entity
public class Tracker {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String path;
    @Column
    private String session;

    public Tracker() {
    }

    public Tracker(String path, String session) {
        this.path = path;
        this.session = session;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tracker tracker = (Tracker) o;

        if (id != tracker.id) return false;
        if (path != null ? !path.equals(tracker.path) : tracker.path != null) return false;
        return session != null ? session.equals(tracker.session) : tracker.session == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (session != null ? session.hashCode() : 0);
        return result;
    }
}
